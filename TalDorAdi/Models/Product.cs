﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TalDorAdi.Models
{
    public class Product
    {
        [Required]
        public string productID { get; set; }

        [Required]
        public string productType { get; set; }

        [Required]
        public string productCompany { get; set; }

        [Required]
        [RegularExpression("^[0-9]{2}$", ErrorMessage = "* Size must be two digits")]
        public int size { get; set; }

        [Required]
        [RegularExpression("^[0-9]+$", ErrorMessage = "* Price must be a number")]
        public int price { get; set; }

        [Required]
        public string color { get; set; }

        [Required]
        [RegularExpression("^[0-9]+$", ErrorMessage = "* Amount must contain digits")]
        public int initialAmount { get; set; }

    }
}