﻿function UpdateProfile() {
    $("#editForm").submit(function (e) {
        console.log("Form sent!")
        e.preventDefault();
        if ($("#editForm").valid()) {
            let userDetails = {};
            $("#editForm").serializeArray().map(function (item) {
                if (userDetails[item.name]) {
                    if (typeof (userDetails[item.name]) === "string") {
                        userDetails[item.name] = [userDetails[item.name]];
                    }
                    userDetails[item.name].push(item.value);
                } else {
                    userDetails[item.name] = item.value;
                }
            });
            $.ajax({
                type: "PUT",
                url: '/Profile/UpdateProfileDetails',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(userDetails),
                success: EditSuccess
            })
        }
    });
}

function EditSuccess() {
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        background: 'green',
        timer: 3000
    })
    Toast.fire({
        type: 'success',
        title: "Details have been updated successfully!"
    })
}
