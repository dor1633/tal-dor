﻿function getData() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "/Branch/getLocations",
        success: function (data) {
            loadMapScenario(data);
        },
        error: function (data) { console.log(data); }
    });
}


function loadMapScenario(locations) {

    
    var map = new Microsoft.Maps.Map(document.getElementById('myMap'), {});

    locations.map((location, i) => {
        addPoint(map, JSON.parse(location.coordinates).x, JSON.parse(location.coordinates).y, location.title, location.description)
    })

    //Microsoft.Maps.Events.addHandler(map, 'rightclick', clickedPoint);
}

function clickedPoint(e) {

    //check if map is clicked
    if (e.targetType == "map") {
        var title = '';
        var point = new Microsoft.Maps.Point(e.getX(), e.getY());
        var locTemp = e.target.tryPixelToLocation(point);
        var dec = 'dec try';

        $.ajax({
            type: "POST",
            data: JSON.stringify({
                title: title,
                coordinates: JSON.stringify({ x: locTemp.latitude, y: locTemp.latitude}),
                description: dec
            }),
            contentType: "application/json; charset=utf-8",
            url: "/Branch/addLocation",
            success: function (data) {
                loadMapScenario(JSON.parse(data));
            },
            error: function (data) { console.log(data); }
        });


    };

};

function addPoint(map, x, y, title, desc) {
    var locationPosition = map.tryPixelToLocation(new Microsoft.Maps.Point(x, y));
    var pushpin = new Microsoft.Maps.Pushpin(locationPosition, null);
    var infobox = new Microsoft.Maps.Infobox(locationPosition, { title: title, description: desc, visible: false });
    infobox.setMap(map);
    Microsoft.Maps.Events.addHandler(pushpin, 'click', function () {
        infobox.setOptions({ visible: true });
    });
    map.entities.push(pushpin);
}


