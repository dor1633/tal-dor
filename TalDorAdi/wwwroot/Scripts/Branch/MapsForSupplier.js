﻿
function getData() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "/Branch/getLocations",
        success: function (data) {
            loadMapScenario(data);
        },
        error: function (data) { console.log(data); }
    });
}


function loadMapScenario(locations) {


    var map = new Microsoft.Maps.Map(document.getElementById('myMap'), {});

    locations.map((location, i) => {
        addPoint(map, JSON.parse(location.coordinates).x, JSON.parse(location.coordinates).y, location.title, location.description)
    })
}

function addPoint(map, x, y, title, desc) {
    var locationPosition = map.tryPixelToLocation(new Microsoft.Maps.Point(x, y));
    var pushpin = new Microsoft.Maps.Pushpin(locationPosition, null);
    var infobox = new Microsoft.Maps.Infobox(locationPosition, { title: title, description: desc, visible: false });
    infobox.setMap(map);
    Microsoft.Maps.Events.addHandler(pushpin, 'click', function () {
        infobox.setOptions({ visible: true });
    });
    map.entities.push(pushpin);
}



