﻿$('#FilterSearchForm').submit((e) => {
    e.preventDefault();
    var frmData = $("#FilterSearchForm").serialize()
    $.post("SearchByFilter", frmData, ShowFilteredProducts)
});

$('#resetButton').on('click', () => {
    $.get("GetProductByJson", null, DisplayAllProducts);
    clearFields()
    
})

function clearFields() {
    $("#productType").val("Type");
    $("#productCompany").val("Company");
    $("#productSize").val("Size");
    $("#productPrice").val("Price")
    $("#productColor").val("Color")
}

function ShowFilteredProducts(products) {
    var Ptbl = $("#productTable");
    $("#productTable").find("tr:gt(0)").remove();
    for (i = 0; i < products.length; i++) {
        if (products[i].initialAmount != 0) {
            var newRow = "<tr id=" + products[i].productID + ">" +
                "<td class=prodID>" + products[i].productID + "</td>" +
                "<td class=prodtype>" + products[i].productType + "</td>" +
                "<td class=prodcompany>" + products[i].productCompany + "</td>" +
                "<td class=prodsize>" + products[i].size + "</td>" +
                "<td class=prodprice>" + products[i].price + "</td>" +
                "<td class=prodcolor>" + products[i].color + "</td>" +
                "<td class=prodinitialamount>" + products[i].initialAmount + "</td>" +
                "<td>" + "<input type=button value=select onclick=Buy(" + products[i].productID + ")>" + "</td>" +
                "</tr>";
            Ptbl.append(newRow);
        }
    }
    $("#status").text("");
}