﻿var sizes = [30, 32, 34, 36, 38, 40, 42, 44, 46, 48]
var priceRanges = ["0-100", "101-200", "201-300", "301-400", "401-500", "501-600", "601-700", "701-800", "801-900", "901-1000"]
var productTypes = ["shirt", "polo", "shorts", "pants", "sweater", "jacket", "skirts", "shoes"];
var productCompanies = ["Adidas", "Nike", "Puma", "NB", "Solomon"]
var productColors = ["Blue ", "Green ", "Brown", "Purple", "Yellow", "White", "Black", "Gray", "Lime","Red"]

CreateSelectOptions(sizes, "productSize");
CreateSelectOptions(priceRanges, "productPrice");
CreateSelectOptions(productTypes, "productType");
CreateSelectOptions(productCompanies, "productCompany");
CreateSelectOptions(productColors, "productColor");