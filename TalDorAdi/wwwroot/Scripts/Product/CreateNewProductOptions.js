﻿var sizes = [30, 32, 34, 36, 38, 40, 42, 44, 46, 48]
var productTypes = ["shirt", "polo", "shorts", "pants", "sweater", "jacket", "skirts", "shoes"];
var productCompanies = ["Adidas", "Nike", "Puma", "NB", "Solomon"]
var productColors = ["Blue ", "Green ", "Brown", "Purple", "Yellow", "White", "Black", "Gray", "Lime", "Red"]

CreateSelectOptions(sizes, "productSize");
CreateSelectOptions(productTypes, "productType");
CreateSelectOptions(productCompanies, "productCompany");
CreateSelectOptions(productColors, "productColor");