﻿using Microsoft.AspNetCore.Mvc;

namespace TalDorAdi.Controllers
{
    public class ErrorController : Controller
    {
        public ViewResult PageNotFound()
        {
            Response.StatusCode = 404;
            return View();
        }
    }
}
