﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using TalDorAdi.Dal;
using TalDorAdi.Models;

namespace TalDorAdi.Controllers
{
    public class BranchController : Controller
    {
        private readonly DataLayer _context;
        public BranchController(DataLayer context)
        {
            _context = context;
        }
        // GET: Branch
        public ActionResult Index()
        {
            return View();
        }

        public ViewResult ShowMapToSupplier()
        {
            HttpContext.Session.SetString("PageTitle", "Our Branches");
            return View();
        }

        public ViewResult ShowMapToManager()
        {
            HttpContext.Session.SetString("PageTitle", "Our Branches");
            return View();
        }

        [HttpGet]
        public JsonResult getLocations()
        {
            List<Location> objLocations = _context.Locations.ToList<Location>();
            return Json(objLocations);
        }

        [HttpPost]
        public JsonResult addLocation(Location loc)
        {
            List<Location> objLocations =
             (from a in _context.Locations
              where (a.coordinates.Equals(loc.coordinates))
              select a).ToList<Location>();
            
            if (objLocations.Count == 0)
            {
                _context.Locations.Add(loc);

                try
                {
                    _context.SaveChanges();
                }
                catch (DbEntityValidationException ex)
                {
                    ViewBag.Error = TempData["there was problem. please try again"];
                }
            }

            else ViewBag.Error = TempData["There is branch in this place"];
            
            objLocations = _context.Locations.ToList<Location>();

            return Json(objLocations);
        }

    }
}
