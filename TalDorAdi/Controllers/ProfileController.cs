﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using TalDorAdi.Dal;
using TalDorAdi.Models;

namespace TalDorAdi.Controllers
{
    public class ProfileController : Controller
    {
        private readonly DataLayer _context;
        public ProfileController(DataLayer context)
        {
            _context = context;
        }
        public ActionResult ShowProfilePage()
        {
            if (HttpContext.Request.Cookies["Supplier"] != null || HttpContext.Request.Cookies["Manager"] != null)
            {
                string data = null;
                if (HttpContext.Request.Cookies["Supplier"] != null)
                {
                    data = Request.Cookies["Supplier"];
                }

                if (HttpContext.Request.Cookies["Manager"] != null)
                {
                    data = Request.Cookies["Manager"];
                }
            


                User user = new User();
                user.userID = (String)JObject.Parse(data)["userID"];
                user.name = (String)JObject.Parse(data)["name"];
                user.permission = (int)JObject.Parse(data)["permission"];
                user.password = (String)JObject.Parse(data)["password"];
                HttpContext.Session.SetString("PageTitle", "Profile");
                HttpContext.Session.SetString("LogginInUser", user.name);
                return View(user);
            }
            else
            {
                return RedirectToAction("ShowSignInPage", "Auth");
            }
        }

        [HttpPost]
        public ActionResult UpdateProfileDetails(User newUserDetails)
        {
            string userID = newUserDetails.userID;
            string name = newUserDetails.name;
            string password = newUserDetails.password;
            List<User> userToEdit =
               (from x in _context.Users
                where (x.userID.Equals(userID))
                select x).ToList<User>();//find the product in data base
            userToEdit.First().name = name;
            userToEdit.First().password = password;
            User updatedUser = userToEdit.First();
            _context.SaveChanges();//update data base
            userToEdit = _context.Users.ToList<User>();

            string role = "";
            if (HttpContext.Request.Cookies["Supplier"] != null)
            {
                role = "Supplier";
            }

            if (HttpContext.Request.Cookies["Manager"] != null)
            {
                role = "Manager";
            }

            RemoveCookies();
            UpdateCookie(role, updatedUser);
            HttpContext.Session.SetString("LogginInUser", updatedUser.name);
            TempData["succsessMessage"] = "The Profile successfully updated";
            return RedirectToAction("ShowProfilePage", updatedUser);
        }

        [NonAction]
        public void UpdateCookie(string cookieName, User user)
        {
            CookieOptions option = new CookieOptions();
            option.Expires = DateTime.Now.AddMinutes(15);
            HttpContext.Response.Cookies.Delete(cookieName);
            HttpContext.Response.Cookies.Append(cookieName, JsonConvert.SerializeObject(user), option);
        }

        public void RemoveCookies()
        {
            if (HttpContext.Request != null)
            {
                foreach (var cookie in HttpContext.Request.Cookies)
                {
                    Response.Cookies.Delete(cookie.Key);
                }
            }
        }
    }
}
