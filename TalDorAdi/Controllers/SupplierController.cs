﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using TalDorAdi.Dal;
using TalDorAdi.Models;
using TalDorAdi.ViewModel;

namespace TalDorAdi.Controllers
{
    //[Authorize] //Have access only to suppliers
    [Route("[controller]/[action]")]
    [ApiController]
    public class SupplierController : Controller
    {
        private readonly DataLayer _context;
        public SupplierController(DataLayer context)
        {
            _context = context;
        }
        // GET: supplier
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ShowSupplierPage()
        {
            if (HttpContext.Request.Cookies["Supplier"] != null)
            {
                User user = new User();
                String data = Request.Cookies["Supplier"];
                user.userID = (String)JObject.Parse(data)["userID"];
                user.name = (String)JObject.Parse(data)["name"];
                user.permission = (int)JObject.Parse(data)["permission"];
                user.password = (String)JObject.Parse(data)["password"];
                HttpContext.Session.SetString("LogginInUser", user.name);
                HttpContext.Session.SetString("PageTitle", "Recommended"); 
                return View(user);//return view of supplier home page
            }
            else
            {
                return RedirectToAction("ShowSignInPage", "Auth");
            }
        }
        public ActionResult ShowProfilePage()
        {
            if (HttpContext.Request.Cookies["Supplier"] != null || HttpContext.Request.Cookies["Manager"] != null)
            {
                String data = Request.Cookies["Supplier"];
                if (HttpContext.Request.Cookies["Manager"] != null)
                {
                    data = Request.Cookies["Manager"];
                }


                User user = new User();
                user.userID = (String)JObject.Parse(data)["userID"];
                user.name = (String)JObject.Parse(data)["name"];
                user.permission = (int)JObject.Parse(data)["permission"];
                user.password = (String)JObject.Parse(data)["password"];
                HttpContext.Session.SetString("LogginInUser", user.name);
                HttpContext.Session.SetString("PageTitle", "Profile");
                return View(user);
            }
            else
            {
                return RedirectToAction("ShowSignInPage", "Auth");
            }
        }
        public ActionResult ShowSuppliers()
        {
            if (HttpContext.Request.Cookies["Manager"] != null)
            {
                String data = Request.Cookies["Manager"];
                HttpContext.Session.SetString("LogginInUser", (String)JObject.Parse(data)["name"]);
                HttpContext.Session.SetString("PageTitle", "Suppliers");
                return View();
            }
            else
            {
                return RedirectToAction("ShowSignInPage", "Auth");
            }
        }
        [HttpGet]
        public JsonResult GetSuppliersByJson()
        {
            int permission = 0;
            List<User> objSuppliers = (from user in _context.Users
                                       where user.permission.Equals(permission)
                                       select user).ToList<User>();

            return Json(objSuppliers);
        }

        [HttpDelete]
        public JsonResult DeleteSupplierByJson([FromQuery]string id)
        {
            List<User> objSupplier =
               (from x in _context.Users
                where (x.userID.Equals(id.ToString()))
                select x).ToList<User>();
            if (objSupplier.Count == 1)
            {
                _context.Users.Remove(objSupplier[0]);
                _context.SaveChanges();//update data base
            }
            return GetSuppliersByJson();
        }

        [HttpGet]
        public ActionResult ShowMyReservations()
        {
            if (HttpContext.Request.Cookies["Supplier"] != null)
            {
                HttpContext.Session.SetString("PageTitle", "My Chart");
                return View();
            }
            else
            {
                return RedirectToAction("ShowSignInPage", "Auth");
            }
        }

        [HttpGet]
        public JsonResult GetMyReservations()
        {
            String data = Request.Cookies["Supplier"];
            String currentUserID = (String)JObject.Parse(data)["userID"];
            List<Order> objOrders = _context.Orders.ToList<Order>();
            List<Product> objProducts = _context.Products.ToList<Product>();

            var AllOrders = (from ord in objOrders
                                join prod in objProducts on ord.productID equals prod.productID
                                where currentUserID == ord.supplierID
                                select new OrderProductViewModel
                                {
                                    order = ord,
                                    product = prod

                                }).ToList();
            
            return Json(AllOrders);
        }
    }
}
                   
                


            
    

