﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using TalDorAdi.Dal;
using TalDorAdi.Models;
using TalDorAdi.ModelView;
using TalDorAdi.ViewModel;



namespace TalDorAdi.Controllers
{
    //[Authorize]
    public class ManagerController : Controller
    {
        private readonly DataLayer _context;// DB
        private readonly IWebHostEnvironment _webHostEnvironment; // 

        public ManagerController(IWebHostEnvironment webHostEnvironment, DataLayer context)
        {
            _webHostEnvironment = webHostEnvironment;
            _context = context;
        }

        // GET: Manager
        public ViewResult Index()
        {
            return View();
        }

        public ActionResult ShowManagerPage()
        {
            if (HttpContext.Request.Cookies["Manager"] != null)
            {
                String managerDetails = Request.Cookies["Manager"];
                User user = new User();
                user.userID = (String)JObject.Parse(managerDetails)["userID"];
                user.name = (String)JObject.Parse(managerDetails)["name"];
                user.permission = (int)JObject.Parse(managerDetails)["permission"];
                user.password = (String)JObject.Parse(managerDetails)["password"];
                getMostSellingProducts();
                getSupplierBuying();
                HttpContext.Session.SetString("LogginInUser", user.name);
                HttpContext.Session.SetString("PageTitle", "Dashboard");
                return View(user);//return view of manager home page
            }
            else
            {
                return RedirectToAction("ShowSignInPage", "Auth");
            }
        }

        // Get The top 7 products by sales history 
        [NonAction]
        public void getMostSellingProducts() {
            List<Order> objOrders = _context.Orders.ToList<Order>();
            List<Product> objProducts = _context.Products.ToList<Product>();

            // Sum each product with its sales
            var sells = objOrders.GroupBy(ord => ord.productID)
                                 .Select(ord => new { amount = ord.Sum(b => b.amount), productID = ord.Key }).ToList();

            // 
            var mostSellingProducts = (from product in objProducts
                                 join sell in sells
                                 on product.productID equals sell.productID
                                 into result
                                 from newSell in result.DefaultIfEmpty()
                                 select new SellProductViewModel { productType = product.productType, productCompany = product.productCompany, price = newSell != null ? newSell.amount : 0 }).Take(7).ToArray();

            writeToFile("MostSellingProducts.csv", mostSellingProducts);

        }

        // Get The top 7 products by sales history according the suuplier buying
        [NonAction]
        public void getSupplierBuying() {
            List<Order> objOrders = _context.Orders.ToList<Order>();
            List<Product> objProducts = _context.Products.ToList<Product>();
            
            // Sum each product with its sales
            var sells = objOrders.GroupBy(ord => new { ord.supplierID, ord.productID })
                                 .Select(ord => new { amount = ord.Sum(b => b.amount), suppAndProd = ord.Key }).ToList();

            // 
            var suppliersBuying = (from product in objProducts
                                  join sell in sells
                                  on product.productID equals sell.suppAndProd.productID
                                  into result
                                  from newSell in result.DefaultIfEmpty()
                                  select new SupplierProductVM { productType = product.productType, supplierID = newSell != null ? newSell.suppAndProd.supplierID : "no", productCompany = product.productCompany, price = newSell != null ? newSell.amount : 0 }).Take(7).ToArray();

            writeToFile("SupplierBuying.csv", null, suppliersBuying);
        }


        // Write SellProductArray/SupplierProductArray to csv file
        [NonAction]
        public void writeToFile(String fileName, SellProductViewModel[] SellProductArray = null, SupplierProductVM[] SupplierProductArray = null) {
            
            using (System.IO.StreamWriter file =
             new System.IO.StreamWriter(_webHostEnvironment.WebRootPath + "\\csvs\\" + fileName))
            {
                if (SellProductArray != null)
                {
                    file.WriteLine("productType,productCompany,price");
                    foreach (var product in SellProductArray)
                    {
                        file.WriteLine(product.productType + "," + product.productCompany + "," + product.price);
                    }
                }
                else
                {
                    file.WriteLine("productType,productCompany,supplierID,price");
                    foreach (var product in SupplierProductArray)
                    {
                        file.WriteLine(product.productType + "," + product.productCompany + "," + product.supplierID + "," + product.price);
                    }
                }
            }
        }
        
    }
}