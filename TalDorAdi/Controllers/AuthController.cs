﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TalDorAdi.Dal;
using TalDorAdi.Models;

namespace TalDorAdi.Controllers
{
    public class AuthController : Controller
    {
        private readonly DataLayer _context;
        public AuthController(DataLayer context)
        {
            _context = context;
        }
        // GET: Login
        public ViewResult Index() {
            RemoveCookies();
            HttpContext.Session.SetString("PageTitle", "D.A.T Sports");  

            return View("Auth", new User());
        }

        public ViewResult ShowSignInPage() {
            RemoveCookies();
            HttpContext.Session.SetString("PageTitle", "D.A.T Sports");
            return View();
        }

        public ViewResult ShowSignUpPage() {
            HttpContext.Session.SetString("PageTitle", "D.A.T Sports");
            return View();
        }


        [HttpPost]
        public ActionResult SignIn(User user) {
            ModelState.Remove("name");

            // TempData can be used to store temporary data which can be used in the subsequent request.
            TempData["incorrect_details"] = "";

            //If the form has been validated,  user authentication is done
            if (ModelState.IsValid) {
                
                // Search the user in DB
                //DataLayer Mdal = new DataLayer();
                List<User> userFromDb =
                       (from x in _context.Users
                        where ((x.userID.Equals(user.userID)) && (x.password.Equals(user.password)))
                        select x).ToList<User>();
                
                // If the user exists in DB
                if (userFromDb.Count == 1) {

                    // permmision 1 in DB = the user is manager.
                    if (userFromDb[0].permission == 1) {
                        CreateCookie("Manager", userFromDb[0]);
                        return RedirectToAction("ShowManagerPage", "Manager");
                    } else {
                        CreateCookie("Supplier", userFromDb[0]);
                        return RedirectToAction("ShowSupplierPage", "Supplier");
                    }
                } else {
                    TempData["incorrect_details"] = "User or password are wrong!";
                    return RedirectToAction("ShowSignInPage", "Auth");
                }
            } else {
                //If the form was not validated the user stays on the login page with the data it has entered
                ModelState.Clear();
                return RedirectToAction("ShowSignInPage", "Auth");
            }
        }

        [HttpPost]
        public ActionResult SignUp(User user) {
            
            TempData["Message"] = "";
            
            if (ModelState.IsValid) {

                // Search if the userID already exists in DB
                List<User> exists =
                (from x in _context.Users
                 where (x.userID.Equals(user.userID.ToString()))
                 select x).ToList<User>();

                // If doesn't exists, save it in DB
                if (exists.Count == 0) {
                    _context.Users.Add(user);
                    _context.SaveChanges();
                    ViewBag.Message = "";
                    CreateCookie("Supplier", user);
                    return RedirectToAction("ShowSupplierPage", "Supplier");
                } else {
                    TempData["Message"] = "ID already exists..";
                    return RedirectToAction("ShowSignUpPage", "Auth");
                }
            } else {
                TempData["Message"] = "Invalid params...";
                return RedirectToAction("ShowSignUpPage", "Auth");
            }
        }

        // This function create cookie with the current user and his role(Manager/Supplier)
        [NonAction]
        public void CreateCookie(String role, User user) {
            CookieOptions option = new CookieOptions();
            option.Expires = DateTime.Now.AddMinutes(15);
            HttpContext.Response.Cookies.Append(role, JsonConvert.SerializeObject(user), option);
        }

        // This function clear all the cookies
        [NonAction]
        public void RemoveCookies() {
            if (HttpContext.Request != null) {
                foreach (var cookie in HttpContext.Request.Cookies)
                {
                    Response.Cookies.Delete(cookie.Key);
                }
            }
        }
    }


}
