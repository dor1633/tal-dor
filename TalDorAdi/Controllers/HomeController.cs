﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace TalDorAdi.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ViewResult Index()
        {
            //RemoveCookies();
            HttpContext.Session.SetString("PageTitle", "D.A.T Sports");
            return View("ShowHomePage");
        }

        public ViewResult ShowHomePage()
        {
            //RemoveCookies();
            HttpContext.Session.SetString("PageTitle", "D.A.T Sports");
            return View();
        }

        public void RemoveCookies()
        {
            if (HttpContext.Request != null)
            {
                foreach (var cookie in HttpContext.Request.Cookies)
                {
                    Response.Cookies.Delete(cookie.Key);
                }
            }
        }

    }
}