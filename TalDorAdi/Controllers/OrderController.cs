﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using TalDorAdi.Dal;
using TalDorAdi.Models;
using TalDorAdi.ViewModel;

namespace TalDorAdi.Controllers
{

    [Route("[controller]/[action]")]
    [ApiController]
    public class OrderController : Controller
    {

        private readonly DataLayer _context;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public OrderController(IWebHostEnvironment webHostEnvironment, DataLayer context)
        {
            _webHostEnvironment = webHostEnvironment;
            _context = context;
        }
        // GET: Order
        public ViewResult Index()
        {
            return View();
        }

        // web service!!
        public async Task<JsonResult> GetCurrentDollarJson(string currCoin)
        {
            try {
                HttpClient client = new HttpClient();
                var responseString = await client.GetStringAsync("https://api.exchangeratesapi.io/latest?symbols=" + currCoin);
                return Json(responseString);
            }
            catch(Exception ex)
            {
                return Json(1);
            }
            
        }

        public ActionResult ShowOrders()//return view of show orders page
        {
            if (HttpContext.Request.Cookies["Manager"] != null)
            {
                HttpContext.Session.SetString("PageTitle", "Orders");
                return View();
            }
            else
            {
                return RedirectToAction("ShowSignInPage", "Auth");
            }
        }

        // Join
        private IEnumerable<OrderProductViewModel> GetOrderedProducts()
        {
            List<Order> objOrders = _context.Orders.ToList<Order>();
            List<Product> objProducts = _context.Products.ToList<Product>();
            var orderProductViewModel = from product in objProducts
                                        join order in objOrders
                                        on product.productID equals order.productID
                                        into result
                                        from newOrder in result.DefaultIfEmpty()
                                        select new OrderProductViewModel { product = product, order = newOrder };
            return orderProductViewModel;
        }
        public JsonResult GetOrderByJson()//return json with all the current orders
        {
            var orderedProducts = GetOrderedProducts();
            return Json(orderedProducts);
        }
        public ActionResult ShowReservationPage()
        {
            if (HttpContext.Request.Cookies["Supplier"] != null)
            {
                String data = Request.Cookies["Supplier"];
                User user = new User();
                user.userID = (String)JObject.Parse(data)["userID"];
                user.name = (String)JObject.Parse(data)["name"];
                user.permission = (int)JObject.Parse(data)["permission"];
                user.password = (String)JObject.Parse(data)["password"];
                HttpContext.Session.SetString("LogginInUser", user.name);
                HttpContext.Session.SetString("PageTitle", "New Order");
                return View(user);
            }
            else
            {
                return RedirectToAction("ShowSignInPage", "Auth");
            }


        }
        public JsonResult MakeOrder()
        {
            Order o = new Order();
            o.productID = Request.Form["txtProductID"];//Taking information from the form
            o.supplierID = Request.Form["txtUserID"];
            o.amount = Int32.Parse(Request.Form["txtOption"]);
            o.orderDate = DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss tt"); ;

            ViewBag.Error = TempData[""];

            //DataLayer Pdal = new DataLayer();
            List<Product> objProducts =
             (from x in _context.Products
              where (x.productID.Equals(o.productID))//Finding the desired product in data base
              select x).ToList<Product>();

            if ((objProducts[0].initialAmount >= o.amount))//If there is enough in stock you can to buy
            {
                List<Order> objO = _context.Orders.ToList<Order>();
                //find the max orderID
                var maxOrderID = -1;
                foreach (Order order in objO)
                {
                    if (Int32.Parse(order.orderID) > maxOrderID)
                    {
                        maxOrderID = Int32.Parse(order.orderID);
                    }
                }
                o.orderID = (maxOrderID + 1).ToString();
                //o.productName = objProducts[0].productName;
                objProducts[0].initialAmount -= o.amount;
                _context.Orders.Add(o);//add order to data base

                try
                {
                    _context.SaveChanges();
                }
                catch (DbEntityValidationException ex)
                {
                    ViewBag.Error = TempData["there was problem. please try again"];
                }
            }
            else ViewBag.Error = TempData["not enough in stock"];
            List<Order> objOrders = _context.Orders.ToList<Order>();
            objProducts = _context.Products.ToList<Product>();
            groupUsersAndProducts();
            return Json(objProducts);//return json with all the current products 
        }

        [HttpDelete]
        public JsonResult DeleteOrderByJson([FromQuery]string id)
        {
            List<Order> objOrders =
               (from x in _context.Orders
                where (x.orderID.Equals(id.ToString()))
                select x).ToList<Order>();
            if (objOrders.Count == 1)
            {
                _context.Orders.Remove(objOrders[0]);
                _context.SaveChanges();//update data base
            }
            return GetOrderByJson();
        }

        [HttpPut]
        public JsonResult ChangeOrderAmount([FromQuery]string id, [FromQuery]string orderAmount, [FromQuery]string newAmount)
        {
            List<Order> objOrders =
               (from x in _context.Orders
                where (x.orderID.Equals(id.ToString()))
                select x).ToList<Order>();
            if (objOrders == null) return null;
            var productID = objOrders.First().productID;
            //change the product amount left
            List<Product> productToEdit =
             (from x in _context.Products
              where (x.productID.Equals(productID))//Finding the desired product in data base
              select x).ToList<Product>();
            // current amount = DB amount + amount that were ordered(return the bought) - the new amount bought;
            productToEdit.First().initialAmount = productToEdit.First().initialAmount + Int32.Parse(orderAmount) - Int32.Parse(newAmount);
            //update the order amount
            objOrders.First().amount = Int32.Parse(newAmount);
            _context.SaveChanges();//update data base

            return GetOrderByJson();
        }
        [HttpPost]
        public JsonResult SearchOrder([FromQuery]string productID, [FromQuery]string productName, [FromQuery]string supplierID)
        {
            var orderedProducts = GetOrderedProducts();
            List<OrderProductViewModel> results = null;


            if (productID == "All" && productName == "All" && supplierID == null)
            {
                return Json(orderedProducts);
            }

            results =
                (from x in orderedProducts
                where (x.order != null) && ((productID != "All" ? x.product.productID.Equals(productID) : true) && (productName != "All" ? x.product.productType.Equals(productName) : true)
                && (supplierID != null ? (x.order != null && x.order.supplierID.Equals(supplierID)) : true))
                select x).ToList();

            return Json(results);
        }

        private void groupUsersAndProducts()
        {
            IEnumerable<string> suppliersIds =
               (from x in _context.Orders
                select x.supplierID).ToList().Distinct();

            IEnumerable<Order> orders =
               (from x in _context.Orders
                select x).ToList().Distinct();

            IEnumerable<string> products =
               (from x in _context.Products
                select x.productID);

            List<string> usedProducts = new List<string>();

            foreach (string supplierId in suppliersIds)
            {
                var str = "";
                foreach (string productId in products)
                {
                    var order = orders.FirstOrDefault(order => order.productID == productId && order.supplierID == supplierId);
                    int existSign;
                    if (order != null)
                    {
                        existSign = 1;
                    }
                    else
                    {
                        existSign = 0;
                    }
                    if (str.Length == 0)
                    {
                        str += existSign;
                    }
                    else
                    {
                        str += "," + existSign;
                    }
                }
                usedProducts.Add(str);
            }

            updateOrdersHistory(suppliersIds.ToArray(), products.ToArray(), usedProducts.ToArray());
        }

        private void updateOrdersHistory(string[] suppliersIds, string[] products, string[] usedProducts)
        {
            string fileName = "OrdersHistory.csv";
            var joinIds = String.Join(",", products.ToArray());
            using (System.IO.StreamWriter file =
             new System.IO.StreamWriter(_webHostEnvironment.WebRootPath + "\\csvs\\" + fileName))
            {
                file.WriteLine("userid," + joinIds);
                for (int supplierIndex = 0; supplierIndex < suppliersIds.Length; supplierIndex++)
                {
                    file.WriteLine(suppliersIds[supplierIndex] + "," + usedProducts[supplierIndex]);
                }
            }

        }
    }


}
