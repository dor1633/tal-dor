﻿using System;

namespace TalDorAdi.ViewModel
{
    public class SellProductViewModel
    {
        public int price { get; set; }
        public String productType { get; set; }
        public String productCompany { get; set; }
    }
}