﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TalDorAdi.ModelView
{
    public class SupplierProductVM
    {
        public int price { get; set; }
        public String productType { get; set; }
        public String productCompany { get; set; }
        public String supplierID { get; set; }
    }
}
