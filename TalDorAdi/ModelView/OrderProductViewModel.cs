﻿using TalDorAdi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TalDorAdi.ViewModel
{
    public class OrderProductViewModel
    {
        public Order order { get; set; }
        public Product product { get; set; }
    }
}