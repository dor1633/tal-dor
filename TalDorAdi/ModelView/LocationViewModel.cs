﻿using System.Collections.Generic;
using TalDorAdi.Models;

namespace TalDorAdi.ModelView
{
    public class LocationViewModel
    {
        public Location location { get; set; }
        public List<Location> locations { get; set; }
    }
}
