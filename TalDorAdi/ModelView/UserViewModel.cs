﻿using System.Collections.Generic;
using TalDorAdi.Models;

namespace TalDorAdi.ViewModel
{
    public class UserViewModel
    {
        public Product product{ get; set; }
        public List<Product> products{ get; set; }
        public User user { get; set; }
        public List<User> users { get; set; }

    }
}