﻿using TalDorAdi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TalDorAdi.ViewModel
{
    public class OrderViewModel
    {
        public Order order { get; set; }
        public List<Order> orders { get; set; }
    }
}