﻿using Microsoft.EntityFrameworkCore;
using TalDorAdi.Models;

namespace TalDorAdi.Dal
{
    public class DataLayer : DbContext
    {
        public DataLayer(DbContextOptions<DataLayer> options)
            : base(options)
        {
        }
        public DbSet<User> Users { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Location> Locations { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().ToTable("tblUsers");
            modelBuilder.Entity<Product>().ToTable("tblProducts");
            modelBuilder.Entity<Order>().ToTable("tblOrders");
            modelBuilder.Entity<Location>().ToTable("tblLocations");
        }
    }
}
